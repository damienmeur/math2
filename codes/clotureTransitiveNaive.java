public Relation clotureTransitiveNaive(Relation relationInitiale){
	Relation relationTemp=relationInitiale.deepCopy();
	Relation relationRPlus=relationInitiale.deepCopy();
	int cardinal=relationInitiale.getCardinal();
	for(int i=0;i<cardinal;i++){//O(n^(4) --> ca craint
		relationTemp=relationTemp.relationComposee(relationInitiale);//O(n^(3))
		relationRPlus=relationTemp.union(relationRPlus);
	}
	return relationRPlus;
}
